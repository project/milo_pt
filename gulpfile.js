/**
 * @file
 * Gulp task file.
 *
 * @link http://gulpjs.com/ See GulpJS for more information @endlink.
 */

var gulp       = require('gulp'),
    sass       = require('gulp-sass'),
    concat     = require('gulp-concat'),
    uglify     = require('gulp-uglify'),
    minifyCSS  = require('gulp-minify-css');

// Compile the SCSS files.
gulp.task('sass', function() {
  gulp.src('./scss/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./css'));
});

// Combine all CSS files and minify.
gulp.task('css-min', function() {
  gulp.src(['./css/**/*.css', './css/*.css', '!./css/all.min.css'])
    .pipe(concat('all.min.css'))
    .pipe(minifyCSS())
    .pipe(gulp.dest('./css'));
});

// Combine all JS files and minify.
gulp.task('js-min', function() {
  gulp.src(['./js/vendor/modernizr/modernizr.js', './js/*.js', '!./js/all.min.js'])
    .pipe(concat('all.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./js'));
} );

// Watch files for changes.
gulp.task('watch', function() {
  gulp.watch('./js/*.js', ['js-min']);
  gulp.watch(['./scss/*.scss', './scss/**/*.scss'], ['sass', 'css-min']);
});

gulp.task('default', ['sass', 'css-min', 'js-min']);
