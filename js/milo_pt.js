/**
 * @file
 * Theme logic and UX enhancements.
 */

(function($) {

Drupal.behaviors.miloPT = {

  attach: function(context, settings) {
    var $tabToggle = $('.site-header__page-tabs:not(.js-processed)', context);
    if ($tabToggle.length) {
      $tabToggle.click(function() {
        $(this).toggleClass('site-header__page-tabs--open');
      }).addClass('js-processed');
    }

    var $userToggle = $('.site-header__user-navigation:not(.js-processed)', context);
    if ($userToggle.length) {
      $userToggle.click(function() {
        $(this).toggleClass('site-header__user-navigation--open');
      }).addClass('js-processed');

      $('body').keyup(function(e) {
        if (e.which === 27) {
          $userToggle.removeClass('site-header__user-navigation--open');
        }
      }).click(function(e) {
        if (e.target !== $userToggle[0]) {
          $userToggle.removeClass('site-header__user-navigation--open');
        }
      });
    }
  }

};

})(jQuery);
