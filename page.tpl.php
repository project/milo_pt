<header class="site-header">
  <img class="site-header__logo" src="<?php print $logo; ?>" />

  <nav class="site-header__page-tabs">
    <div class="site-header__page-tabs__tabs-wrapper">
      <?php if (!empty($tabs['#primary']) && !empty($user->uid)): ?>
        <?php print render($tabs); ?>
      <?php else: ?>
        <ul class="tabs primary">
          <!-- @todo !! -->
          <li<?php if ($_GET['q'] == 'node/add/h5p-content'): ?> class="active"<?php endif; ?>>
            <?php if (!empty($user->uid)): ?>
              <?php print l(t("Create"), 'node/add/h5p-content', array(
                'attributes' => array(
                  'class' => array('add-content'),
                ),
              )); ?>
            <?php else: ?>
              <?php print l(t("Create"), 'user/login', array(
                'attributes' => array(
                  'class' => array('add-content'),
                ),
                'query' => array(
                  'destination' => 'node/add/h5p-content',
                ),
              )); ?>
            <?php endif; ?>
          </li>
        </ul>
      <?php endif; ?>
    </div>
  </nav>

  <?php if (isset($user_menu)): ?>
    <nav class="site-header__user-navigation user-navigation">
      <div class="site-header__user-navigation__links-wrapper">
        <?php print render($user_menu); ?>
      </div>
    </nav>
  <?php endif; ?>
</header>

<div class="site-content">
  <?php if ($page['highlighted']): ?>
    <div class="site-content__highlighted-messages highlighted-messages">
      <?php print render($page['highlighted']); ?>
    </div>
  <?php endif; ?>

  <?php if ($messages): ?>
    <div class="site-content__site-messages site-messages">
      <?php print $messages; ?>
    </div>
  <?php endif; ?>

  <a id="main-content"></a>

  <?php print render($title_prefix); ?>
  <?php if ($title): ?>
    <h1 class="site-content__page-title page-title"><?php print $title; ?></h1>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <?php if ($action_links): ?>
    <ul class="site-content__action-links">
      <?php print render($action_links); ?>
    </ul>
  <?php endif; ?>

  <?php print render($page['content']); ?>
</div>

<footer class="site-footer">
  <?php print render($page['footer']); ?>
</footer>
